package org.addcel.util;

import android.app.ProgressDialog;
import android.content.Context;

public class Dialogos {
private static ProgressDialog progress;
	
	public static ProgressDialog makeDialog(Context con, String title, String msj){
		progress = new ProgressDialog(con);
		progress.setTitle(title);
		progress.setMessage(msj);
		progress.setIndeterminate(true);
		progress.show();
		return progress;
	}
	
	public static void closeDialog(){
		if(progress.isShowing()){
			progress.dismiss();
		}
	}
	
	
}
