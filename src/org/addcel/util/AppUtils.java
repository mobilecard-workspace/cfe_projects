package org.addcel.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;
import android.util.Xml.Encoding;

public class AppUtils {
	
	private static final String LOG = "AppUtils";
	
	
	public static String getVersionName(Context context, Class<?> cls) 
    {
      try {
        ComponentName comp = new ComponentName(context, cls);
        PackageInfo pinfo = context.getPackageManager().getPackageInfo(comp.getPackageName(), 0);
        return pinfo.versionName;
      } catch (android.content.pm.PackageManager.NameNotFoundException e) {
        return null;
      }
    }
	
//	public static ArrayAdapter<ItemCatalogoDTO> createCatalogAdapter(Context c, List<ItemCatalogoDTO> list) {
//		
//		ArrayAdapter<ItemCatalogoDTO> adapter = new ArrayAdapter<ItemCatalogoDTO>(c, android.R.layout.simple_spinner_item, list);
//		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//		
//		return adapter;
//	}
	
	public static LinkedList<String> getAniosVigencia() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);
		
		LinkedList<String> anios = new LinkedList<String>();
		
		int amount = anio + 7;
		
		for (int i = anio; i < amount; i++) {
			anios.add("" + i);
		}
		
		return anios;
	}
	
	public static String getKey() {
		Long seed = new Date().getTime();
		
		String key = String.valueOf(seed).substring(0, 8);
		Log.d(LOG, "SENSITIVE KEY: " + key);
		
		return key;
	}
	
	public static String buildUrl(String url, String[] names, String[] values, int numParams) {
		
		if (numParams > 0) {
			List <BasicNameValuePair> params = new ArrayList<BasicNameValuePair>(numParams);
			
			for (int i = 0; i < numParams; i++) {
				params.add(new BasicNameValuePair(names[i], values[i]));
			}
			
			if (!url.endsWith("?")) {
				url += "?";
			}
			
			url += URLEncodedUtils.format(params, Encoding.UTF_8.name());
			
		}
			
		return url;		
	}
}
