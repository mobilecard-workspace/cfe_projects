package org.addcel.util;

import java.util.Arrays;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;


public class UrlBuilder {
	
	/**
	 * 
	 * @param url Direcci&oacute;n del Request
	 * @param params Arreglo de par&aacute;metros del Request
	 * @return URL formateada
	 */
	
	public static String getRequest(String url, BasicNameValuePair... params) {
		if (! url.endsWith("?"))
			url += "?";
		
		return url += URLEncodedUtils.format(Arrays.asList(params), "UTF-8");
	}

}
