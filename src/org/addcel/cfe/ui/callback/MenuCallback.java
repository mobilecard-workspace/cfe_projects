package org.addcel.cfe.ui.callback;

public interface MenuCallback {
	public void onLogin(String response, String login, String key);
	public void onLogout();
	public void onReferenciaCaptured(String referencia);

}
