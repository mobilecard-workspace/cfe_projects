package addcel.cfe.presenter;

import java.util.concurrent.TimeUnit;

import org.addcel.cfe.dto.DatosPago;
import org.addcel.cfe.ui.callback.PagoCallback;
import org.addcel.util.Text;

import com.squareup.phrase.Phrase;

import addcel.cfe.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;

public class PagoPresenter {
	
	private Context context;
	private PagoCallback callback;
	private String numReferencia;
	private DatosPago datosPago;
	private ProgressDialog pDialog;
	private AlertDialog pagoDialog;
	
	public PagoPresenter(Context context, PagoCallback callback, String numReferencia) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.callback = callback;
		this.numReferencia = numReferencia;
//		
	}
	
	public String getNumReferencia() {
		return numReferencia;
	}
	
	public void setNumReferencia(String numReferencia) {
		this.numReferencia = numReferencia;
	}
	
	public DatosPago getDatosPago() {
		return datosPago;
	}
	
	public void setDatosPago(DatosPago datosPago) {
		this.datosPago = datosPago;
	}
	
	private ProgressDialog getpDialog() {
		return getpDialog("");
	}
	
	private ProgressDialog getpDialog(CharSequence msg) {
		
		if (pDialog == null) {
			pDialog = new ProgressDialog(context);
	        pDialog.setIndeterminate(true);
	        pDialog.setMax(100);
	        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	        pDialog.setCancelable(true);
		}
		if (!TextUtils.isEmpty(msg))
			pDialog.setMessage(msg);
		
		return pDialog;
	}
	
	public void getDatosAsync() {
		getpDialog("Obteniendo datos...").show();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				getpDialog().dismiss();
				callback.onDatosGotten(
						new DatosPago(4716.4, 754.62, 5471.02, 0.15, 5471.17));
			}
		}, TimeUnit.SECONDS.toMillis(2));
		
	}
	
	public void postPagoAsync() {
		getpDialog("Ejecutando pago...").show();
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				getpDialog().dismiss();
				
				callback.onPagoFinished("Su pago ha sido exitoso|00000000|" 
				+ Text.formatCurrency(datosPago.getTotal(), true));
			}
		}, TimeUnit.SECONDS.toMillis(3));
	}
	
	public AlertDialog getPagoDialog(String msg) {
		String[] result = TextUtils.split(msg, "\\|");
		
		CharSequence res = Phrase.from("{mensaje} \n\nTotal: {total} \nAutorización: {autorizacion}")
				.put("mensaje", result[0]).put("autorizacion", result[1]).put("total", result[2])
				.format();
		
		if (pagoDialog == null) {
		
			Builder builder = new Builder(context)
							.setTitle(R.string.txt_pago_title)
							.setPositiveButton(android.R.string.ok, 
									new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									((Activity) context).finish();
								}
							});
			
			pagoDialog = builder.create();
		}
		
		pagoDialog.setMessage(res);
		
		return pagoDialog;
		
	}

}
