package addcel.cfe;

import org.addcel.android.crypto.Crypto;
import org.addcel.cfe.ui.callback.MenuCallback;
import org.addcel.util.Text;

import com.dm.zbar.android.scanner.ZBarConstants;

import butterknife.ButterKnife;
import butterknife.OnClick;
import addcel.cfe.presenter.MenuPresenter;
import addcel.cfe.ui.PagoActivity;
import addcel.cfe.ui.Session;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MenuActivity extends Activity implements MenuCallback {
	
	private MenuPresenter presenter;
	
	private static final int ZBAR_SCANNER_REQUEST = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		ButterKnife.inject(this);
		
		presenter = new MenuPresenter(this, this);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		invalidateOptionsMenu();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_logout:
			if (!Session.get().isLoggedIn())
				presenter.getLoginDialog().show();
			else
				presenter.getLogoutDialog().show();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (Session.get().isLoggedIn()) {
			menu.getItem(0).setTitle("Cerrar sesión");
		} else {
			menu.getItem(0).setTitle("Iniciar sesión");
		}
		
		return super.onPrepareOptionsMenu(menu);
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		
		case ZBAR_SCANNER_REQUEST:
			
			if (resultCode == RESULT_OK) {
				
				String referencia = data.getStringExtra(ZBarConstants.SCAN_RESULT);
				onReferenciaCaptured(referencia);
				
			} else if (resultCode == RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				
				if (!TextUtils.isEmpty(error))
					Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
			}
			
			break;

		default:
			break;
		}
	}
	
	@OnClick(R.id.button_capturar)
	void getReferenciaPago() {
		if (Session.get().isLoggedIn()) {
			presenter.launchCapturaReferencia();
		} else {
			Toast
			.makeText(this, "Inicia sesión para completar tu pago.", Toast.LENGTH_LONG)
			.show();
		}
	}

	@Override
	public void onLogin(String response, String login, String key) {
		// TODO Auto-generated method stub
		if (response == null)
			Toast.makeText(this, "Ocurrió un error al procesar la petición.", Toast.LENGTH_LONG).show();
		else {
			response = Crypto.aesDecrypt(Text.parsePass(key), response);
			Log.d(MenuCallback.class.getSimpleName(), response);
			presenter.processResult(response, login, key);
		}
		
		invalidateOptionsMenu();
	}

	@Override
	public void onLogout() {
		// TODO Auto-generated method stub
		invalidateOptionsMenu();
	}

	@Override
	public void onReferenciaCaptured(String referencia) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, PagoActivity.class);
		intent.putExtra("num_servicio", referencia);
		startActivity(intent);
	}

}
