package addcel.cfe.ui;

import org.addcel.cfe.dto.DatosPago;
import org.addcel.cfe.ui.callback.PagoCallback;
import org.addcel.util.Text;

import addcel.cfe.R;
import addcel.cfe.presenter.PagoPresenter;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.OnTextChanged.Callback;

public class PagoActivity extends Activity implements PagoCallback{
	
	@InjectView(R.id.text_servicio) TextView servicioView;
	@InjectView(R.id.text_total) TextView totalView;
	@InjectView(R.id.text_energia) TextView energiaView;
	@InjectView(R.id.text_iva) TextView ivaView;
	@InjectView(R.id.text_facturacion) TextView facturacionView;
	@InjectView(R.id.text_redondeo) TextView redondeoView;
	@InjectView(R.id.text_pwd) EditText passwordText;
	@InjectView(R.id.text_cvv) EditText cvvText;
    private PagoPresenter presenter;
    private boolean passwordValido, cvvValido;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pago);
		ButterKnife.inject(this);
		
		presenter = new PagoPresenter(this, this, 
				getIntent().getStringExtra("num_servicio"));
		
		servicioView.setText(presenter.getNumReferencia());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.pago, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_pagar:
			if (validate()) {
				presenter.postPagoAsync();
			}
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@OnTextChanged(value = R.id.text_pwd, callback = Callback.AFTER_TEXT_CHANGED)
	void validatePassword(Editable s) {
		if (TextUtils.isEmpty(s)) {
			passwordText.setError("Ingresa contraseña");
			passwordValido = false;
		} else {
			if (TextUtils.getTrimmedLength(s) < 8) {
				passwordText.setError("Longitud mínima 8 caracteres");
				passwordValido = false;
			} else {
				passwordValido = true;
			}
		}
	}
	
	@OnTextChanged(value = R.id.text_cvv, callback = Callback.AFTER_TEXT_CHANGED)
	void validateCvv(Editable s) {
		if (TextUtils.isEmpty(s)) {
			cvvText.setError("Ingresa dígito verificador");
			cvvValido = false;
		} else {
			if (TextUtils.getTrimmedLength(s) < 3) {
				cvvText.setError("Longitud mínima 3 caracteres");
				cvvValido = false;
			} else {
				cvvValido = true;
			}
		}
	}
	
	@OnClick(R.id.button_obtener)
	void getDatos() {
		presenter.getDatosAsync();
	}
	
	
	private boolean validate() {
		
		if (presenter.getDatosPago() == null) {
			Toast.makeText(PagoActivity.this,
					"Obtener datos de Pago...", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		if (!passwordValido) {
			Toast.makeText(PagoActivity.this,
					"Captura una contraseña válida", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		if (!cvvValido) {
			Toast.makeText(PagoActivity.this,
					"El dígito verificador no es válido", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return true;
	}

	@Override
	public void onPagoFinished(String datos) {
		// TODO Auto-generated method stub
		presenter.getPagoDialog(datos).show();
	}

	@Override
	public void onDatosGotten(DatosPago datos) {
		// TODO Auto-generated method stub
		presenter.setDatosPago(datos);
		
		energiaView.setText(Text.formatCurrency(presenter.getDatosPago().getEnergia(), true));
		ivaView.setText(Text.formatCurrency(presenter.getDatosPago().getIva(), true));
		facturacionView.setText(Text.formatCurrency(presenter.getDatosPago().getFacPeriodo(), true));
		redondeoView.setText(Text.formatCurrency(presenter.getDatosPago().getRedondeo(), true));
		totalView.setText(Text.formatCurrency(presenter.getDatosPago().getTotal(), true));
	}
	
	
}
