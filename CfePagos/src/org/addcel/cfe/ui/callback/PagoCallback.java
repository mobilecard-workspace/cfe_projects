package org.addcel.cfe.ui.callback;

import org.addcel.cfe.dto.DatosPago;

public interface PagoCallback {

	public void onDatosGotten(DatosPago datos);
	public void onPagoFinished(String datos);
}
