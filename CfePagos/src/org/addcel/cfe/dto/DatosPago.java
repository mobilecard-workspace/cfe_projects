package org.addcel.cfe.dto;

public class DatosPago {
	
	private double energia;
	private double iva;
	private double facPeriodo;
	private double redondeo;
	private double total;
	
	public DatosPago(double energia, double iva, double facPeriodo,
			double redondeo, double total) {
		super();
		this.energia = energia;
		this.iva = iva;
		this.facPeriodo = facPeriodo;
		this.redondeo = redondeo;
		this.total = total;
	}

	public double getEnergia() {
		return energia;
	}

	public void setEnergia(double energia) {
		this.energia = energia;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getFacPeriodo() {
		return facPeriodo;
	}

	public void setFacPeriodo(double facPeriodo) {
		this.facPeriodo = facPeriodo;
	}

	public double getRedondeo() {
		return redondeo;
	}

	public void setRedondeo(double redondeo) {
		this.redondeo = redondeo;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
