package org.addcel.cfe.dto;


import org.addcel.android.crypto.Crypto;
import org.addcel.util.DeviceUtils;

import com.google.gson.annotations.SerializedName;

public class LoginData {
	
	private String login;
	private String password;
	private String imei;
	@SerializedName("passwordS")
	private String passwordSha;
	private String tipo;
	private String software;
	private String modelo;
	private String key;
	
	public LoginData() {
		// TODO Auto-generated constructor stub
	}
	
	public LoginData(String login, String password, String imei) {
		this.login = login;
		this.password = password;
		this.imei = imei;
		this.passwordSha = Crypto.sha1(password);
		this.tipo = DeviceUtils.getTipo();
		this.software = DeviceUtils.getSWVersion();
		this.modelo = DeviceUtils.getModel();
		this.key = imei;
	}
	
	public String getLogin() {
		return login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getImei() {
		return imei;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setImei(String imei) {
		this.imei = imei;
	}
	

}
