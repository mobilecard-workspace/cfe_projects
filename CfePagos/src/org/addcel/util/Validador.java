package org.addcel.util;

public class Validador {

	public static boolean esCorreo(String correo){
		return (correo.matches("^[_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@+([_a-zA-Z0-9-]+\\.)*[a-zA-Z0-9-]{2,200}\\.[a-zA-Z]{2,6}$"));
	}
	
	/**
	 * 
	 * @param String rfc
	 * @return boolean
	 */
	public static boolean esRFC(String rfc) {
		return rfc.matches("[A-Z,�,&]{3,4}[0-9]{2}[0-1][0-9][0-3][0-9][A-Z,0-9]?[A-Z,0-9]?[0-9,A-Z]?");
	}
	
	/**
	 * @param String curp
	 * @return boolean
	 */
	public static boolean esCurp(String curp) {
		
		return curp.matches("[A-Z][A,E,I,O,U,X][A-Z]{2}[0-9]{2}[0-1][0-9][0-3][0-9][M,H][A-Z]{2}[B,C,D,F,G,H,J,K,L,M,N,�,P,Q,R,S,T,V,W,X,Y,Z]{3}[0-9,A-Z][0-9]");
	}
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito VISA valido (13 o 16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito VISA a esReferenciaValida
	 */
	public static boolean esTarjetaDeCreditoVisa(String tarjetaCredito){
		return (tarjetaCredito.matches("^4[0-9]{12}(?:[0-9]{3})?$"));
	}
	
	
	/**
	 * Verifica si un texto es un n�mero de tarjeta de cr�dito MASTER CARD valido (16 digitos)
	 * @param tarjetaCredito Numero de la tarjeta de credito MASTER CARD a esReferenciaValida
	 */
	public static boolean esTarjetaDeCreditoMasterCard(String tarjetaCredito){
		return (tarjetaCredito.matches("^5[1-5][0-9]{14}$"));
	}
}
