package org.addcel.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

public class DeviceUtils {

	public static String getIMEI(Context ctx){
		String imei = "";
			
		try{
			imei = ((TelephonyManager)ctx.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
			
			if (imei == null){
				imei = Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
			}
		}catch(Exception e){
			imei = Settings.System.getString(ctx.getContentResolver(), Settings.System.ANDROID_ID);
			Log.e("getImei",e.getMessage());
		}
		
		return imei;
	}
	
	public static String getSWVersion(){
		return Build.VERSION.RELEASE;
	}
	
	public static String getModel(){
		return Build.MODEL;
	}
	
	public static String getTipo(){
		return Build.MANUFACTURER;
	}
	
}
