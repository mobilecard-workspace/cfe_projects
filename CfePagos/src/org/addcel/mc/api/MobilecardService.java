package org.addcel.mc.api;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface MobilecardService {
	
	@FormUrlEncoded
	@POST("/AddCelBridge/Servicios/adc_userLogin.jsp")
	Response login(@Field("json") String param) throws Exception;

}
