package addcel.cfe.presenter;

import org.addcel.android.crypto.Crypto;
import org.addcel.cfe.dto.LoginData;
import org.addcel.cfe.dto.LoginResult;
import org.addcel.cfe.ui.callback.MenuCallback;
import org.addcel.mc.api.MobilecardService;
import org.addcel.util.DeviceUtils;
import org.addcel.util.Text;

import com.dm.zbar.android.scanner.ZBarScannerActivity;

import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;
import addcel.cfe.R;
import addcel.cfe.ui.Session;
import addcel.gson.GsonFactory;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.TextKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnTextChanged;
import butterknife.OnTextChanged.Callback;

public class MenuPresenter {
	
	private Context context;
	private MenuCallback callback;
	private AlertDialog loginDialog,logoutDialog, tipoCapturaDialog, capturaManualDialog;
	private ProgressDialog pDialog;
	private static MobilecardService mobilecard;
	
	public MenuPresenter(Context context, MenuCallback callback) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.callback = callback;
	}
	
	public void launchCapturaReferencia() {
		if (isCameraAvailable())
			getTipoCapturaDialog().show();
		else
			getCapturaManualDialog().show();
	}
	
	public ProgressDialog getpDialog(CharSequence msg) {
		if (pDialog == null) {
			pDialog = new ProgressDialog(context);
	        pDialog.setIndeterminate(true);
	        pDialog.setCancelable(true);
		}
		if (!TextUtils.isEmpty(msg))
			pDialog.setMessage(msg);
		
		return pDialog;
	}
	
	public ProgressDialog getpDialog() {
		return getpDialog("");
	}
	
	private AlertDialog getTipoCapturaDialog() {
		if (tipoCapturaDialog == null) {
			Builder builder = new Builder(context);
			builder.setMessage(R.string.txt_tipo_captura);
			builder.setNegativeButton(R.string.txt_captura_teclado, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					getCapturaManualDialog().show();
				}
			});
			builder.setPositiveButton(R.string.txt_captura_escanear, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					launchScanner(0);
				
				}
			});
			
			tipoCapturaDialog = builder.create();
		}
		
		return tipoCapturaDialog;
	}
	
	private AlertDialog getCapturaManualDialog() {
		
		if (capturaManualDialog == null) {
			
				View view = LayoutInflater.from(context).inflate(R.layout.view_captura_manual, null);
				final ViewHolder holder = new ViewHolder(view);
				
				Builder builder = new Builder(context);
				builder.setMessage(R.string.txt_captura_msg);
				builder.setView(view);
				builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
						if (holder.esReferenciaValida) {
							String referencia = holder.textReferencia.getText().toString().trim();
//								Limpiamos contenido del editable
							TextKeyListener.clear(holder.textReferencia.getEditableText());
							callback.onReferenciaCaptured(referencia);
						}
					}
				});
				
				capturaManualDialog = builder.create();

			}
			
			return capturaManualDialog;
	}
	
	private void launchScanner(int requestCode) {
		
		Intent intent = new Intent(context, ZBarScannerActivity.class);
		((Activity) context).startActivityForResult(intent, requestCode);
	}
	
	private boolean isCameraAvailable() {
		PackageManager pm = context.getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}
	
	private void loginAsync(final String request, final String login, final String key) {
		
		new AsyncTask<Void, Void, String>() {
			
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				getpDialog("Iniciando Sesión...");
			}
			
			@Override
			protected String doInBackground(Void... params) {
				// TODO Auto-generated method stub
				try {
					
					Response response = getMobilecard().login(request);
					return new String(((TypedByteArray) response.getBody()).getBytes());
					
				} catch(Exception e) {
					Log.e(MobilecardService.class.getSimpleName(), "Error", e);
					return null;
				}
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				getpDialog().dismiss();
				callback.onLogin(result, login, key);
			}
		}.execute();
	}
	
	public AlertDialog getLoginDialog() {
		if (loginDialog == null) {
			
			View view = LayoutInflater.from(context).inflate(R.layout.view_login, null);
			final LoginHolder holder = new LoginHolder(view);
			
			Builder builder = new Builder(context).setMessage("Inicia Sesión Mobilecard")
					.setView(view).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							if (holder.isLoginValido() && holder.isPasswordValido()) {
								
								LoginData data = new LoginData(
										holder.loginText.getText().toString().trim(),
										holder.passwordText.getText().toString().trim(),
										DeviceUtils.getIMEI(context));
								
								String request = GsonFactory.get().toJson(data);
								
								request = Crypto.aesEncrypt(
										Text.parsePass(
												holder.passwordText.getText().toString().trim()), request);
								request = Text.mergeStr(request,
										holder.passwordText.getText().toString().trim());
								
								loginAsync(request, holder.loginText.getText().toString().trim(), 
										holder.passwordText.getText().toString().trim());
								
								
							} else {
								Toast.makeText(context, "Valida los datos ingresados",
										Toast.LENGTH_SHORT).show();
							}
						}
					});
			
			loginDialog = builder.create();
		}
		
		return loginDialog;
	}
	
	public AlertDialog getLogoutDialog() {
		if (logoutDialog == null) {
			Builder builder = new Builder(context).setMessage("¿Deseas cerrar sesión?")
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Session.get().logout();
							dialog.dismiss();
							callback.onLogout();
						}
					})
					.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.cancel();
						}
					});
			
			logoutDialog = builder.create();
		}
		
		return logoutDialog;
	}
	
	public static synchronized MobilecardService getMobilecard() {
		if (mobilecard == null) {
			
			RestAdapter.Log log = new RestAdapter.Log() {
				
				@Override
				public void log(String arg0) {
					// TODO Auto-generated method stub
					Log.d(MobilecardService.class.getSimpleName(), arg0);
				}
			};
			
			RestAdapter adapter = new RestAdapter.Builder().setLog(log)
					.setLogLevel(LogLevel.FULL)
					.setEndpoint("http://50.57.192.214:8080/").build();
			
			mobilecard = adapter.create(MobilecardService.class);
		}
		
		return mobilecard;
	}
	
	public void processResult(String result, String login, String password) {
		if (result != null) {
			LoginResult res = GsonFactory.get().fromJson(result, LoginResult.class);
			switch (res.getResultado()) {
			case 1:
				Session.get().setId(Long.parseLong(parseMessage(res.getMensaje())[1]));
				Session.get().setLogin(login);
				Session.get().setPassword(password);
				Toast.makeText(context, parseMessage(res.getMensaje())[0], Toast.LENGTH_LONG).show();
				break;
			case 99:
				Session.get().setId(Long.parseLong(parseMessage(res.getMensaje())[1]));
				Session.get().setLogin(login);
				Session.get().setPassword(password);
				Toast.makeText(context, parseMessage(res.getMensaje())[0], Toast.LENGTH_LONG).show();
				break;
			default:
				Session.get().logout();
				Toast.makeText(context, res.getMensaje(), Toast.LENGTH_LONG).show();
				break;
			}
		}
	}
	
	private String[] parseMessage(String msg) {
		return TextUtils.split(msg, "\\|");
	}
	
	
	static class ViewHolder {
		
		@InjectView(R.id.text_referencia)
		EditText textReferencia;
		boolean esReferenciaValida;
		
		public ViewHolder(View view) {
			ButterKnife.inject(this, view);
		}
		
		@OnTextChanged(value = R.id.text_referencia, callback = Callback.AFTER_TEXT_CHANGED)
		void setTextError(Editable s) {
			if (TextUtils.isEmpty(s)) {
				textReferencia.setError("Ingresa una referencia válida");
				esReferenciaValida = false;
			} else {
				if (!TextUtils.isDigitsOnly(s)) {
					textReferencia.setError("Referencia no válida");
					esReferenciaValida = false;
				}  else {
					if (TextUtils.getTrimmedLength(s) <= 5) {
						textReferencia.setError("Longitud mínima 6 caracteres");
						esReferenciaValida = false;
					} else {
						esReferenciaValida = true;
					}
				}
			}
		}
	}
	
	static class LoginHolder {
		
		@InjectView(R.id.text_login) 
		EditText loginText;
		@InjectView(R.id.text_password)
		EditText passwordText;
		
		private boolean loginValido, passwordValido;
		
		
		public LoginHolder(View view) {
			ButterKnife.inject(this, view);
		}
		
		public boolean isLoginValido() {
			return loginValido;
		}
		
		public void setLoginValido(boolean loginValido) {
			this.loginValido = loginValido;
		}
		
		public boolean isPasswordValido() {
			return passwordValido;
		}
		
		public void setPasswordValido(boolean passwordValido) {
			this.passwordValido = passwordValido;
		}
		
		@OnTextChanged(value = R.id.text_login, callback = Callback.AFTER_TEXT_CHANGED)
		void validaLogin(Editable s) {
			if (TextUtils.isEmpty(s)) {
				loginText.setError("Ingresa nombre de usuario");
				loginValido = false;
			} else {
				if (TextUtils.getTrimmedLength(s) < 4) {
					loginText.setError("Longitud mínima 4 caracteres");
					loginValido = false;
				} else {
					loginValido = true;
				}
			}
		}
		
		@OnTextChanged(value = R.id.text_password, callback = Callback.AFTER_TEXT_CHANGED)
		void validaPassword(Editable s) {
			if (TextUtils.isEmpty(s)) {
				passwordText.setError("Ingresa contraseña");
				passwordValido = false;
			} else {
				if (TextUtils.getTrimmedLength(s) < 8) {
					passwordText.setError("Longitud mínima 8 caracteres");
					passwordValido = false;
				} else {
					passwordValido = true;
				}
			}
		}
	}

}
