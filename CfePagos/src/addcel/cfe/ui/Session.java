package addcel.cfe.ui;

import org.addcel.android.crypto.AddcelCrypto;

import android.text.TextUtils;

public final class Session {
	
	private long id;
	private String login;
	private String password;
	
	private static Session instance;
	
	private Session() {
		
	}
	
	public static synchronized Session get() {
		if (instance == null)
			instance = new Session();
		
		return instance;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return AddcelCrypto.decryptHard(password);
	}
	
	public void setPassword(String password) {
		this.password = AddcelCrypto.encryptHard(password);
	}
	
	public boolean isLoggedIn() {
		if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password) &&
			id > 0L) 
			return true;
		else
			return false;
	}
	
	public void logout() {
		id = 0L;
		login = "";
		password = "";
	}

}
