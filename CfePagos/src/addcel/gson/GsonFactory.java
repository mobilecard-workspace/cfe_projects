package addcel.gson;

import com.google.gson.Gson;

public final class GsonFactory {
	
	private static Gson instance;
	
	private GsonFactory() {
		
	}
	
	public static synchronized Gson get() {
		if (instance == null) {
			instance = new Gson();
		}
		
		return instance;
	}

}
